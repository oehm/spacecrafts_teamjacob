#include "CompositorListenerSpeedEffect.h"
#include <OgreTechnique.h>

CompositorListenerSpeedEffect::CompositorListenerSpeedEffect() : mValue(0.0f)
{
}

void CompositorListenerSpeedEffect::setValue(const float pValue)
{
	//mValue anteilig berechnen (max. 0.2)
	mValue = pValue * -0.10f;
}

void CompositorListenerSpeedEffect::notifyMaterialSetup(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat)
{
	mat->getBestTechnique()->getPass(pass_id)->getFragmentProgramParameters()->setNamedConstant("blurfactor", mValue);
}

void CompositorListenerSpeedEffect::notifyMaterialRender(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat)
{
	mat->getBestTechnique()->getPass(pass_id)->getFragmentProgramParameters()->setNamedConstant("blurfactor", mValue);
}