#include "..\include\ObjectiveKillCraft.h"


ObjectiveKillCraft::ObjectiveKillCraft(Spacecraft* target) :
	Objective(),
	mTarget(target)
{
}


ObjectiveKillCraft::~ObjectiveKillCraft()
{
}


bool ObjectiveKillCraft::isCompleted()
{
	return mTarget->getIsDead();
}