 #ifndef __AIController_h_
 #define __AIController_h_ 

#include "SpacecraftController.h"
#include "Path.h"
#include "LuaScriptManager.h"


class AIController: public SpacecraftController
{
public:
	AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, CommandCenter* c1, Path& patrol);

	void setHumanSpacecraft(Spacecraft* human){
		mHumanSpacecraft = human;
	}

	virtual void update(float delta);

	bool getRechargeFlag(){ return getSpacecraft()->getRechargeFlag(); }
	void setRechargeFlag(bool flag) { getSpacecraft()->setRechargeFlag(flag); }
	
	bool getEnemyVis(); //function should return if enemy is visible for (in sight of) the ship (also false if enemy (our ship) is out of Grid!) and set enemyOutOfSight to 0.0f!!!!!!!!

	float getHPofShip(){ return getSpacecraft()->getHealth(); }
	int getAmmoOfShip() { return getSpacecraft()->getAmmo(); }

	bool wasEnemyVis() { return (enemyOutOfSightTime <= 5.8f); }

	bool flyToBase(); //lets the plane fly to base and returns true when the Ship reached the Base! false when on the way

	bool heal(); //heals the ship and returns true if full of health. false oterwise

	void chaseAndAttackEnemy(); //lets the Ship chase the Enemy

	void seekEnemy(); //lets the Ship seek the Enemy (no Conditions needed if enemy is in sight or time out of sight is exceeded!!! LUA Script makes the Decisions!!)
	//and add delta to enemyOutOfSightTime every update!!!!

	void patrol(); //lets the Ship follow the Patrol Path if on Path! Otherwise the Ship should calculate a Path to the PatrolPath (A*!) and follow.

private:
	float flyToPath(Path* target);
	float flyToSpot(Vector3 pos, float maxDist);
	
	Spacecraft* mHumanSpacecraft;
	Spacecraft* mThisCraft;

	Path* mActivePath;
	Path* mLastPath;
	Path mPatrolPath;

	float mCurrentParam;

	float fieldOfView;
	float enemyOutOfSightTime;

	float maxShootAngle;

	float delta;

	luabind::object mRoot;

	int craftid;
	String craftName;

	btClock cl;
	unsigned long ti;
	String status;
};


#endif