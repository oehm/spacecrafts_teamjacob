#pragma once
#include "Objective.h"
#include "Spacecraft.h"

class ObjectiveKillCraft :
	public Objective
{
private:
	Spacecraft* mTarget;

public:
	ObjectiveKillCraft(Spacecraft* target);

	virtual ~ObjectiveKillCraft();

	virtual bool isCompleted();
};

