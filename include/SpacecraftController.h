 #ifndef __SpacecraftController_h_
 #define __SpacecraftController_h_ 

#include "Spacecraft.h"
#include"CommandCenter.h"

#include "NavigationGraph.h"
#include "NavigationNode.h"

class Path;

class SpacecraftController
{
public:
	SpacecraftController(Spacecraft* spacecraft, CommandCenter* c1);
	virtual ~SpacecraftController();

	virtual void update(float delta) = NULL;

	Spacecraft* getSpacecraft()
	{
		return mSpacecraft;
	}


	CommandCenter* getCommandCenter()
	{
		return mCommandcenter;
	}

public:
	//function should return if ship is on Spot to heal! false if not
	bool getOnSpot();

	Ogre::Vector3 seek(const Ogre::Vector3& target) const;
	Ogre::Vector3 arrive(const Ogre::Vector3& target) const;
	Ogre::Vector3 pursue() const;
	Ogre::Vector3 obstacleAvoidance() const;
	Ogre::Vector3 wallAvoidance() const;
	Ogre::Vector3 followPath(const Path& path, float& currentParam) const;

	Spacecraft* mSpacecraft;
	CommandCenter* mCommandcenter;

protected:
	void setSpacecraft(Spacecraft* craft)
	{
		mSpacecraft = craft;
	}
};

#endif