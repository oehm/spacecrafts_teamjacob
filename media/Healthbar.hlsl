sampler meterSample : register(s0);

float4 main(float2 texCoord: TEXCOORD0) : COLOR0
{
	float meterValue = 2;
	//float3 Color;

	float4 tex = tex2D(meterSample, texCoord);
	float2 position = texCoord - float2(0.5, 0.5);
	float angle = saturate((atan2(-position.y, -position.x) / (3.1416 * 2) - 0.5 + meterValue) * 50);
	tex.r = 1; // min(tex.a, angle);
	tex.g = 0;
	tex.b = 0;
	tex.a = min(tex.a, angle);
	return tex;
}
