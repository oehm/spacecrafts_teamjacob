#pragma once

#include <OgreCompositorInstance.h>

class CompositorListenerHitEffect : public Ogre::CompositorInstance::Listener
{
private:
	float mValue;
	float mStep;
public:
	CompositorListenerHitEffect();
	void notifyMaterialSetup(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat) override;
	void notifyMaterialRender(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat) override;
};
