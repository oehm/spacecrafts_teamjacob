 #ifndef __HumanController_h_
 #define __HumanController_h_ 

#include <OISEvents.h>
#include <OISKeyboard.h>

#include "SpacecraftController.h"

/// Maps user input to steering command.
class HumanController: public SpacecraftController
{
	static const float RESPAWN_COOLDOWN;

public:
	HumanController(Spacecraft* spacecraft, CommandCenter* c1);

	virtual void update(float delta);

	bool keyPressed(const OIS::KeyEvent &arg);
    bool keyReleased(const OIS::KeyEvent &arg);

private:
	float mSteer;
	float mMove;

	float mRespawnTimer;

};

#endif