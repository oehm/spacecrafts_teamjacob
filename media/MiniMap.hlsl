sampler2D input : register(s0);

float4 minimap(float4 pos : POSITION, float2 uv : TEXCOORD0) : COLOR
{
	float4 Color;
	Color = tex2D(input, uv.xy)*1.2f;
	//Color.rgb = (Color.r + Color.g + Color.b) / 3.0f;
	//if (Color.r<0.2 || Color.r>0.9) Color.r = 0; else Color.r = 1.0f;
	//if (Color.g<0.2 || Color.g>0.9) Color.g = 0; else Color.g = 1.0f;
	//if (Color.b<0.2 || Color.b>0.9) Color.b = 0; else Color.b = 1.0f;
	return Color;
}
