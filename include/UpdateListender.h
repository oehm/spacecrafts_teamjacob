#pragma once
#include "FileWatcher.h"
#include <iostream>
#include "LuaScriptManager.h"

class UpdateListener : public FW::FileWatchListener
{
public:
	UpdateListener(scripting::Manager* m) : manager(m) { };
	void handleFileAction(FW::WatchID watchid, const FW::String& dir, const FW::String& filename,
		FW::Action action)
	{
		std::cout << "DIR (" << dir + ") FILE (" + filename + ") has event " << action << std::endl;
		manager->runScriptFile(dir +"\\"+ filename);
	};
private:
	scripting::Manager* manager;
};

