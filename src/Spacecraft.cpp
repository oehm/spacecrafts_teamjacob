#include "StdAfx.h"


#include "Spacecraft.h"
#include "GameApplication.h"
#include "GameConfig.h"
#include "DebugOverlay.h"
#include "MathUtil.h"
#include "WorldUtil.h"

using namespace Ogre;

float Spacecraft::MAX_SPEED = 60.0f;
float Spacecraft::MAX_ANGULAR_SPEED = Math::PI * 0.5f;

float Spacecraft::MAX_LINEAR_ACCELERATION = 0.5f * 60.0f;
float Spacecraft::MAX_ANGULAR_ACCELERATION = 20.0f * 60.0f;
const float Spacecraft::SHOOT_COOLDOWN_DURATION = 0.25f;
const float Spacecraft::HEALTH_RECOVERY_RATE = 0.1f;

const float Spacecraft::AMMO_RECHARGE_COOLDOWN = 0.8f;
const int Spacecraft::MAX_AMMO = 15;


float Spacecraft::DRAG = 0.001f;

static int offset = 0;

#define BIT(x) (1<<(x))


Spacecraft::Spacecraft(int id, const Ogre::String& name, SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const Ogre::Vector3& position, const Ogre::String texture, const int PlanePos) :
	mBody(NULL),
	mNode(NULL),
	mEngineParticleSystem(NULL),
	mDamageParticleSystem(NULL),
	mLinearSteering(Vector3::ZERO),
	mAngularSteering(0.0f),
	mRadius(5.0f),
	mHealth(1.0f),
	mAmmo(MAX_AMMO),
	mShieldTimer(0.0f),
	mShootTimer(0.0f),
	mAmmoRechargeTimer(0.0f),
	//mRespawnTimer(0.0f),
	isRecharging(false),
	isDead(false),
	mId(id),
	mName(name),
	mTexture(texture)
	//mFirstPersonSpacesship(FirstPersonSpacesship)
{
	GameConfig& config = GameConfig::getSingleton();
	MAX_SPEED = config.getValueAsReal("Spacecraft/MaxSpeed");
	MAX_ANGULAR_SPEED = config.getValueAsReal("Spacecraft/MaxAngularSpeed");
	MAX_LINEAR_ACCELERATION = config.getValueAsReal("Spacecraft/MaxLinearAcceleration");
	MAX_ANGULAR_ACCELERATION = config.getValueAsReal("Spacecraft/MaxAngularAcceleration");
	
	//Vector3 size(mRadius*2.0f, mRadius*2.0f, mRadius*2.0f);
 
 	// mesh creation
 	Entity *entity = sceneMgr->createEntity(name, "wasphunter.mesh");			    
 	entity->setCastShadows(true);
 	// we need the bounding box of the box to be able to set the size of the Bullet-box
 		
 	AxisAlignedBox boundingB = entity->getBoundingBox();
	Vector3 meshSize = boundingB.getSize();

	entity->setMaterialName("spacecraft/" + mTexture);
	mNode = sceneMgr->getRootSceneNode()->createChildSceneNode();
	//mNode->setScale(mRadius * 2.0f, mRadius * 2.0f, mRadius* 2.0f);

	SceneNode* ship = mNode->createChildSceneNode(Vector3::UNIT_Z * 0.1f);
 	ship->attachObject(entity);
 	//ship->scale(0.75f, 0.75f, 0.75f);
		
 	// use a sphere as collision shape
	OgreBulletCollisions::SphereCollisionShape *sceneBoxShape = new OgreBulletCollisions::SphereCollisionShape(mRadius);

 	mBody = new OgreBulletDynamics::RigidBody(name + "_body", world, WorldUtil::FILTER_DYNAMIC_SPACECRAFT, WorldUtil::FILTER_ALL);

	Quaternion orientaion(1, 0, 0, 0);
	mBody->setShape(mNode,
 				sceneBoxShape,
 				0.0f,			// dynamic body restitution
 				0.4f,			// dynamic body friction
 				1.0f, 			// dynamic bodymass
 				position,		// starting position of the box
				orientaion);	// orientation of the box

	// limit body rotation to y axis.
	mBody->getBulletRigidBody()->setAngularFactor(btVector3(0.0f, 1.0f, 0.0f));
	mBody->getBulletRigidBody()->setUserPointer(this);
	mBody->getBulletRigidBody()->setDamping(0.0f, 0.4f);

	// never disable the physics of spacecraft.
	mBody->disableDeactivation();

	mEngineParticleSystem = sceneMgr->createParticleSystem(name + "_engine_particles", "Space/Engine");
	mNode->attachObject(mEngineParticleSystem);

	mDamageParticleSystem = sceneMgr->createParticleSystem(name + "_damage_particles", "Space/Damage");
	mNode->attachObject(mDamageParticleSystem);
	mDamageParticleSystem->setEmitting(false);

	
	mShield = mNode->createChildSceneNode(name + "_shield");
	mShieldEntity = sceneMgr->createEntity(name + "_shield", "sphere.mesh");
	mShieldEntity->setCastShadows(false);

	// clone shild texture since we change the alpha setting per ship
	Ogre::MaterialPtr shieldMaterial = Ogre::MaterialManager::getSingleton().getByName("spacecraft/shield/" + texture);
	shieldMaterial->clone(name + "_shildTexture");
	mShieldEntity->setMaterialName(name + "_shildTexture");

	mShield->attachObject(mShieldEntity);
	Vector3 sphereSize = mShieldEntity->getBoundingBox().getSize();
	mShield->setScale(mRadius * 2 / sphereSize.x, mRadius * 2 / sphereSize.y, mRadius * 2 / sphereSize.z);

	Ogre::SceneNode* PlayerSquereNode = mNode->createChildSceneNode(name + "_PlayerSquere");
	mPlayerSquere = sceneMgr->createEntity(name + "_Squere_" + texture, Ogre::SceneManager::PT_PLANE);
	mPlayerSquere->setMaterialName(mTexture);
	PlayerSquereNode->attachObject(mPlayerSquere);
	PlayerSquereNode->setScale(0.09, 0.09, 0);
	PlayerSquereNode->setOrientation(Ogre::Quaternion(Radian(Degree(-90)), Ogre::Vector3(1, 0, 0)));
	PlayerSquereNode->setPosition(Ogre::Vector3(0, PlanePos, 0));

	/*if (IsFirstPersonSpaceship())
	{
		mHealthbarNode = mNode->createChildSceneNode(name + "_BG");
		mHealthBG = sceneMgr->createEntity(name + "_BG", Ogre::SceneManager::PT_PLANE);
		mHealthBG->setMaterialName("BarBackground");
		mHealthbarNode->attachObject(mHealthBG);
		mHealthbarNode->setScale(0.03, 0.015, 1);
		mHealthbarNode->setOrientation(Ogre::Quaternion(Radian(Degree(-180)), Ogre::Vector3(1, 0, 0)));
		mHealthbarNode->setPosition(Ogre::Vector3(0, 6.5f, -0.6));
	}*/

	BillboardSet* billboardSetH = sceneMgr->createBillboardSet(name + "_Health", 10);
	BillboardSet* billboardSetA = sceneMgr->createBillboardSet(name + "_Ammo", 10);
	billboardSetH->setMaterialName("spacecraft/health");
	billboardSetA->setMaterialName("spacecraft/ammo");
	
	mHealthBillboard = billboardSetH->createBillboard(Vector3(0.0f, 5.0f, 0.0f));
	mAmmoBillboard = billboardSetA->createBillboard(Vector3(0.0f, 6.0f, 0.0f));
	mHealthBillboard->setDimensions(5.0f, 1.0f);
	mAmmoBillboard->setDimensions(5.0f, 1.0f);

	mNode->attachObject(billboardSetH);
	mNode->attachObject(billboardSetA);

	DebugOverlay::getSingleton().addTextBox(mName, "", 0, offset, 200, 20, ColourValue(0.0f, 1.0f, 0.0f));

	offset += 20;
}

/*bool Spacecraft::IsFirstPersonSpaceship()
{
	return false; // mFirstPersonSpacesship; //nicht mehr notwendig
}*/

Spacecraft::~Spacecraft()
{
	mDamageParticleSystem->setEmitting(false);
	mBody->detachFromParent();
	delete mBody;
}


Ogre::Quaternion Spacecraft::getOrientation() const
{
	return mBody->getWorldOrientation();
}

void Spacecraft::setOrientation(const Ogre::Quaternion& orientation)
{
	mBody->setOrientation(OgreBtConverter::to(orientation));

	// I'm not sure why but we also have to reset the centerofmasstransform in order apply the new orientation.
	btTransform transform = mBody->getBulletRigidBody()->getCenterOfMassTransform();
	transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(getPosition()));
	transform.setRotation(OgreBulletCollisions::OgreBtConverter::to(orientation));

	// apply new values
	mBody->getBulletRigidBody()->setCenterOfMassTransform(transform);
}

Vector3 Spacecraft::getDirection() const
{
	Quaternion orientation = mNode->getOrientation();
	return orientation.zAxis();
}

float Spacecraft::getYaw() const
{
	Quaternion orientation = mNode->getOrientation();
	return orientation.getYaw().valueRadians();
}

Vector3 Spacecraft::getLinearVelocity() const
{
	Vector3 vel = mBody->getLinearVelocity();
	vel.y = 0.0f;
	return vel;
}

float Spacecraft::getAngularVelocity() const
{
	btRigidBody* rb = mBody->getBulletRigidBody();
	return rb->getAngularVelocity().y();
}

void Spacecraft::setSteeringCommand(const Ogre::Vector3& linear, float angular)
{
	if (!isDead)
	{
		mLinearSteering = linear;
		//mLinearSteering.y = 0.0f;	// disable y
		mAngularSteering = angular;
	}
	else
	{
		mLinearSteering = -getLinearVelocity();
		mAngularSteering = MathUtil::mapAngle(-getAngularVelocity());
	}
}


void Spacecraft::setSteeringCommand(const Ogre::Vector3& linear)
{
	if (!isDead)
	{
		mLinearSteering = linear;
		//mLinearSteering.y = 0.0f;	// disable y

		Vector3 dir = getDirection();
		Vector3 desiredDir = getLinearVelocity() + mLinearSteering;

		if (desiredDir.length() > 0.001)
		{
			desiredDir.normalise();

			float desiredRotation = atan2(-desiredDir.x, desiredDir.z);
			float currentRotation = atan2(-dir.x, dir.z);
			mAngularSteering = currentRotation - desiredRotation;
			mAngularSteering = MathUtil::mapAngle(mAngularSteering - getAngularVelocity());
		}
		else
		{
			mAngularSteering = 0.0f;
		}
	}
	else
	{
		mLinearSteering = -getLinearVelocity();
		mAngularSteering = MathUtil::mapAngle(-getAngularVelocity());
	}
}

void Spacecraft::shoot()
{
	if (mShootTimer <= 0.0f && mAmmo >0 && !isDead)
	{
		mAmmo--;
		mShootTimer = SHOOT_COOLDOWN_DURATION;
		GameApplication::getSingleton().createRocket(getPosition() + getDirection() * 10.0f, getDirection(), mTexture);
	}
}

void Spacecraft::hit()
{
	if (!isDead)
	{
		mShieldTimer = 1.0f;
		mHealth -= 0.45f;
	}
}

void Spacecraft::update(float delta)
{
	updatePhysics(delta);
	updateLogic(delta);
	updateEffects(delta);

	// Debug i
	Vector3 vel = this->getLinearVelocity();
	DebugOverlay::getSingleton().setTextf(mName, "%s: vel: %2.2f, angularvel: %2.2f", mName.c_str(), vel.length(), getAngularVelocity());
}

void Spacecraft::onCollision(ICollider* collider)
{
	mEngineParticleSystem->setEmitting(false);

}

void Spacecraft::updatePhysics(float delta)
{
	btRigidBody* rb = mBody->getBulletRigidBody();

	Vector3 direction = getDirection();
	Vector3 linearVelocity = getLinearVelocity();
	float angularVelocity = getAngularVelocity();

	// Applies traction impulse so that the spacecraft only moves straight.
	float len = direction.dotProduct(linearVelocity);
	Vector3 tractionImpulse = ((direction * len)  - linearVelocity) * 0.5f;
	rb->applyCentralImpulse(btVector3(tractionImpulse.x, tractionImpulse.y, tractionImpulse.z));

	// apply linear steering
	if (mLinearSteering.length() > MAX_LINEAR_ACCELERATION)
	{
		mLinearSteering.normalise();
		mLinearSteering *= MAX_LINEAR_ACCELERATION;
	}
	rb->applyCentralForce(OgreBtConverter::to(mLinearSteering));

	// apply angular steering
	// Note: multiply mAngularSteering with MAX_ANGULAR_ACCELERATION for faster turning.
	float torque = Math::Clamp((mAngularSteering) * MAX_ANGULAR_ACCELERATION, -MAX_ANGULAR_ACCELERATION, MAX_ANGULAR_ACCELERATION);
	rb->applyTorque(btVector3(0.0f, torque, 0.0f)); 

	// clamp velocity to MAX_SPEED
	if (linearVelocity.length() > MAX_SPEED)
	{
		linearVelocity.normalise();
		linearVelocity *= MAX_SPEED;
		rb->setLinearVelocity(OgreBtConverter::to(linearVelocity));
	}

	// clamp angular velocity to MAX_ANGULAR_SPEED
	rb->setAngularVelocity(btVector3(0.0f, Math::Clamp(angularVelocity, -MAX_ANGULAR_SPEED, MAX_ANGULAR_SPEED), 0.0f));
}

void Spacecraft::updateLogic(float delta)
{
	if (isDead)
	{
		return;
	}
	
	if (mHealth <= 0.0f)
	{
		isDead = true;
	}
	
	mShootTimer = Math::Clamp(mShootTimer - delta, 0.0f, 1.0f);
}

void Spacecraft::updateEffects(float delta)
{
	if (isDead)
	{
		/*if (IsFirstPersonSpaceship())
			if (mHealthbarNode != nullptr)
				mHealthbarNode->detachAllObjects();*/

		mEngineParticleSystem->setEmitting(false); 
		Ogre::TextureUnitState* ptus = mShieldEntity->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0);
		ptus->setAlphaOperation(Ogre::LBX_MODULATE, Ogre::LBS_MANUAL, Ogre::LBS_TEXTURE, 0.0f);
		mDamageParticleSystem->setEmitting(true);
		mHealthBillboard->setDimensions(0.0f, 0.7f);
		mAmmoBillboard->setDimensions(0.0f, 0.7f);
		mPlayerSquere->setVisible(false);

		return;
	}

	Vector3 dir = this->getDirection();
	Vector3 vel = this->getLinearVelocity();

	if (vel.squaredLength() > 1.0f && dir.dotProduct(vel) > 0.0f && !isDead)
	{
		mEngineParticleSystem->setEmitting(true);
	}
	else
	{
		mEngineParticleSystem->setEmitting(false);
	}

	if (mShieldTimer > 0.0f)
	{
		mShieldTimer -= delta;
	}
	else
	{
		mShieldTimer = 0.0f;
	}

	float lengthHealth = 5.0f * mHealth;
	float lengthAmmo = (float) (mAmmo)/3.0f;

	// set shield texture alpha
	Ogre::TextureUnitState* ptus = mShieldEntity->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0);
	ptus->setAlphaOperation(Ogre::LBX_MODULATE, Ogre::LBS_MANUAL, Ogre::LBS_TEXTURE, mShieldTimer * 0.5f);

	mHealthBillboard->setDimensions(lengthHealth, 0.7f);
	mAmmoBillboard->setDimensions(lengthAmmo, 0.7f);
	
	mHealthBillboard->setPosition(2.5f - lengthHealth * 0.5f, 7.0f, -1);
	mAmmoBillboard->setPosition(2.5f - lengthAmmo * 0.5f, 6.0f, -1);

}

bool Spacecraft::heal(float delta)
{
	if (!isDead)
	{
		mAmmoRechargeTimer += delta;
		if (mAmmoRechargeTimer > AMMO_RECHARGE_COOLDOWN && mAmmo < MAX_AMMO)
		{
			mAmmo++;
			mAmmoRechargeTimer = 0.0f;
		}
		mHealth = Math::Clamp(mHealth + delta * HEALTH_RECOVERY_RATE, 0.0f, 1.0f);
	}
	return (mHealth == 1.0f && mAmmo == MAX_AMMO);
}