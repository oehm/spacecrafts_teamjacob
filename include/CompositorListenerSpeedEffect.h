#pragma once

#include <OgreCompositorInstance.h>

class CompositorListenerSpeedEffect : public Ogre::CompositorInstance::Listener
{
private:
	float mValue;
public:
	void setValue(const float pValue);
	CompositorListenerSpeedEffect();
	void notifyMaterialSetup(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat) override;
	void notifyMaterialRender(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat) override;
};
