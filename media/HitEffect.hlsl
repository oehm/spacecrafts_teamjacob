float2 RandomCoord : register(C1);
sampler2D TexSampler : register(S0);
uniform float effect;

float4 main(float4 pos : POSITION, float2 uv : TEXCOORD) : COLOR
{
	float4 color = tex2D(TexSampler, uv);
	
	float2 distance = 0.5 - uv;
	float i = RandomCoord.x * 0.04 - 0.02;
	// Vignette effect in red
	//color.r = effect;
	color.g *= (0.4 + i - dot(distance, distance))  * effect;
	color.b *= (0.4 + i - dot(distance, distance))  * effect;
	//color.r = effect;
	//color.a
	return color;
}
