#pragma once
class Objective
{
protected:
	Objective();

public:
	virtual ~Objective();

	virtual bool isCompleted();

};


