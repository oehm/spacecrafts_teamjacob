#include "stdafx.h"
#include "WorldUtil.h"
#include "GameApplication.h"
#include "Spacecraft.h"
#include "Wall.h"

bool WorldUtil::rayCast(Spacecraft* spacecraft, float lookAheadTime, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter)
{
	const btConvexShape* shape = (const btConvexShape*) spacecraft->getRigidBody()->getShape()->getBulletShape();
	const btVector3 ray = OgreBtConverter::to(spacecraft->getLinearVelocity() * lookAheadTime);

	btTransform from = spacecraft->getRigidBody()->getBulletRigidBody()->getWorldTransform();
	btTransform to = from;
	to.setOrigin(from.getOrigin() + ray);

	return rayCast(shape, from, to, collisionPosition, collisionNormal, collisionFilter);
}

bool WorldUtil::rayCast(const btConvexShape* shape, const btTransform& from, const btTransform& to, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter)
{
	OgreBulletDynamics::DynamicsWorld* physics = GameApplication::getSingleton().getPhysics();

	btCollisionWorld::ClosestConvexResultCallback rayCallback(from.getOrigin(), to.getOrigin());
	rayCallback.m_collisionFilterMask = (short) collisionFilter;

	if (from.getOrigin().distance2(to.getOrigin()) < 0.001f)
	{
		// bullet doesn't like if from and to are too close. return false to avoid division by zero exception
		return false;
	}

	physics->getBulletCollisionWorld()->convexSweepTest(shape, from, to, rayCallback);

	if (rayCallback.hasHit())
	{
		collisionPosition = BtOgreConverter::to(rayCallback.m_hitPointWorld);
		collisionNormal = BtOgreConverter::to(rayCallback.m_hitNormalWorld);
		return true;
	}

	return false;
}

bool WorldUtil::rayCast(const Ogre::Vector3& from, const Ogre::Vector3& to, Ogre::Vector3& collisionPosition, Ogre::Vector3& collisionNormal, CollisionFilter collisionFilter)
{
	OgreBulletDynamics::DynamicsWorld* physics = GameApplication::getSingleton().getPhysics();

	btVector3 fromBt = OgreBtConverter::to(from);
	btVector3 toBt = OgreBtConverter::to(to);

	btCollisionWorld::ClosestRayResultCallback rayCallback(fromBt, toBt);
	rayCallback.m_collisionFilterMask = (short)collisionFilter;

	if (from.squaredDistance(to) < 0.001f)
	{
		// bullet doesn't like if from and to are too close. return false to avoid division by zero exception
		return false;
	}
	
	physics->getBulletCollisionWorld()->rayTest(OgreBtConverter::to(from), OgreBtConverter::to(to), rayCallback);

	if (rayCallback.hasHit())
	{
		collisionPosition = BtOgreConverter::to(rayCallback.m_hitPointWorld);
		collisionNormal = BtOgreConverter::to(rayCallback.m_hitNormalWorld);
		return true;
	}

	return false;
}

const std::vector<Spacecraft*>& WorldUtil::getAllSpacecrafts()
{
	return GameApplication::getSingleton().getSpacecrafts();
}
