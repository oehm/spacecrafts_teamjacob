#include "StdAfx.h"

#include "CommandCenter.h"
#include "WorldUtil.h"

using namespace Ogre;

int CommandCenter::msInstanceCounter = 0;

CommandCenter::CommandCenter(Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const Ogre::Vector3& position, const Ogre::String teamColor):
mPosition(position)
{
	String name = "commandcenter_" + StringConverter::toString(msInstanceCounter++);

	//Entity *entity = sceneMgr->createEntity(name, "command_center.mesh");
	Entity *entity = sceneMgr->createEntity(name, "command_center.mesh");
 	entity->setCastShadows(true);
	entity->setMaterialName("command_center");

	mNode = sceneMgr->getRootSceneNode()->createChildSceneNode();
 	mNode->attachObject(entity);

	mShield = mNode->createChildSceneNode(name + "_shieldCommandCenter");
	mShieldEntity = sceneMgr->createEntity(name + "_shieldCommandCenter", "sphere.mesh");
	mShieldEntity->setCastShadows(false);
	Ogre::MaterialPtr shieldMaterial = Ogre::MaterialManager::getSingleton().getByName("CommandCenter/shield/" + teamColor);
	shieldMaterial->clone(name + "_shildTextureCommandCenter");
	mShieldEntity->setMaterialName(name + "_shildTextureCommandCenter");
	mShield->attachObject(mShieldEntity);
	Vector3 sphereSize = mShieldEntity->getBoundingBox().getSize();
	float radius = 40.0f;
	mShield->setScale(radius * 2 / sphereSize.x, radius * 2 / sphereSize.y, radius * 2 / sphereSize.z);
	mShield->setPosition(Ogre::Vector3(0, -10, 0));
	//e*JW

	Ogre::Vector3 offset(0.0f, entity->getBoundingBox().getHalfSize().y, 0.0f);
	mNode->setPosition(position + offset);

	// after that create the Bullet shape with the calculated size
	mShape = new OgreBulletCollisions::BoxCollisionShape(entity->getBoundingBox().getHalfSize());
    // and the Bullet rigid body
	mBody = new OgreBulletDynamics::RigidBody(name, world, WorldUtil::FILTER_STATIC_OBSTACLES, WorldUtil::FILTER_ALL);
    mBody->setStaticShape(mNode, mShape, 0.1f, 0.0f, position + offset);
	
	btRigidBody* rigidBody = mBody->getBulletRigidBody();
	rigidBody->setUserPointer(this);

	Ogre::SceneNode* healSpotNode[4];
	Ogre::Entity* healSpotEnt[4];

	for (unsigned int i = 0; i < 4; i++)
	{
		Ogre::String spotName = name + "_healspot_";
		std::stringstream ss;
		ss << i;
		spotName += ss.str();

		healSpotNode[i] = mNode->createChildSceneNode(spotName);
		healSpotEnt[i] = sceneMgr->createEntity(spotName, Ogre::SceneManager::PT_PLANE);
		healSpotEnt[i]->setMaterialName("healspot");
		healSpotNode[i]->attachObject(healSpotEnt[i]);
		healSpotNode[i]->setScale(0.10, 0.10, 0);
		healSpotNode[i]->setOrientation(Ogre::Quaternion(Radian(Degree(-90)), Ogre::Vector3(1, 0, 0)));
	}

	healSpotNode[0]->setPosition(Vector3(40.0f, 0.1f, 0.0f) - offset);
	healSpotNode[1]->setPosition(Vector3(0.0f, 0.1f, 40.0f) - offset);
	healSpotNode[2]->setPosition(Vector3(-40.0f, 0.1f, 0.0f) - offset);
	healSpotNode[3]->setPosition(Vector3(0.0f, 0.1f, -40.0f) - offset);
	
}

void CommandCenter::createChareSpots()
{
	mChargeSpots = new NavigationNode*[4];
	mChargeSpots[0] = NavigationGraph::getSingletonPtr()->getNearestNode(mPosition + Vector3(25.0f, 0.0f, 0.0f));
	mChargeSpots[1] = NavigationGraph::getSingletonPtr()->getNearestNode(mPosition + Vector3(-25.0f, 0.0f, 0.0f));
	mChargeSpots[2] = NavigationGraph::getSingletonPtr()->getNearestNode(mPosition + Vector3(0.0f, 0.0f, 25.0f));
	mChargeSpots[3] = NavigationGraph::getSingletonPtr()->getNearestNode(mPosition + Vector3(0.0f, 0.0f, -25.0f));
}

