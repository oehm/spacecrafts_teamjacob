#include "stdafx.h"
#include "StateMachine.h"


State::~State()
{
	for (Transition* transition : mTransitions)
	{
		delete transition;
	}

	mTransitions.clear();
}

StateMachine::~StateMachine()
{
	for (State* state: mStates)
	{
		delete state;
	}

	mStates.clear();
	mCurrentState = NULL;
}

void StateMachine::update(float delta)
{
	if (mCurrentState == NULL)
	{
		return;
	}

	Transition* triggeredTransition = NULL;

	for (Transition* transition : mCurrentState->getTransitions())
	{
		if (transition->isTriggered())
		{
			triggeredTransition = transition;
			break;
		}
	}

	if (triggeredTransition != NULL)
	{
		State* targetState = triggeredTransition->getTargetState();
		switchState(targetState);
	}

	mCurrentState->update(delta);
}

void StateMachine::switchState(State* targetState)
{
	if (mCurrentState != targetState)
	{
		mCurrentState->exit();
		mCurrentState = targetState;
		mCurrentState->enter();

	}
}