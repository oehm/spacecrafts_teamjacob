#include "StdAfx.h"

#include "HumanController.h"

#include "DebugDisplay.h"
#include "OgreConsole.h"
#include "GameApplication.h"
#include "WorldUtil.h"
#include "MathUtil.h"
#include "GameConfig.h"
#include "DebugOverlay.h"

using namespace Ogre;

const float HumanController::RESPAWN_COOLDOWN = 5.0f;



HumanController::HumanController(Spacecraft* spacecraft, CommandCenter* c1):
	SpacecraftController(spacecraft,c1),
	mSteer(0.0f),
	mMove(0.0f),
	mRespawnTimer(0.0f)
{
	DebugOverlay::getSingleton().addTextBox("angle", "", 0, 100, 200, 20, ColourValue(1.0f, 1.0f, 0.0f));

}


void HumanController::update(float delta)
{
	if (getSpacecraft()->getIsDead())
	{
		mRespawnTimer += delta;
		if (mRespawnTimer >= RESPAWN_COOLDOWN)
		{
			//code respawn here;
			Spacecraft* newSpacecraft = GameApplication::getSingleton().createSpacecraft(Vector3(950.0f, 10.0f, -200.0f), "green");
			GameApplication::getSingletonPtr()->deleteHumanSpacecraft(getSpacecraft(),newSpacecraft);
			setSpacecraft(newSpacecraft);

			mRespawnTimer = 0.0f;
		}
	}

	if (GameConfig::getSingleton().getValueAsBool("HumanController/DirectControl"))
	{
		Vector3 direction = mSpacecraft->getDirection();
		Vector3 desiredVelocity = direction * mMove * Spacecraft::MAX_SPEED;
		Vector3 currentVelocity = mSpacecraft->getLinearVelocity();
		float desiredRotation = mSteer * Spacecraft::MAX_ANGULAR_SPEED;
		float currentRotation = mSpacecraft->getAngularVelocity();

		//---- Fixes bug with rotating in the same direction like before (when changing between 'A' and 'D' key) on higher angular_speed settings.
		float ang = 0.0f;
		if (currentRotation <= 0 && desiredRotation > 0){
			ang = MathUtil::mapAngle(desiredRotation + currentRotation);
		}
		else if (desiredRotation < 0 && currentRotation >= 0){
			ang = MathUtil::mapAngle(desiredRotation);
		}
		else{
			ang = MathUtil::mapAngle(desiredRotation - currentRotation);
		}
		mSpacecraft->setSteeringCommand(desiredVelocity - currentVelocity, ang);

		//-------------------------------------------------------------------------

		DebugOverlay::getSingleton().setTextf("angle", "angle: %2.2f des: %2.2f cur: %2.2f", ang, desiredRotation, currentRotation);
		DebugDisplay::getSingleton().drawLine(mSpacecraft->getPosition(), mSpacecraft->getPosition() + desiredVelocity * 1.0f, ColourValue(0.0f, 1.0f, 0.0f));
	}
	else
	{
		Vector3 direction = mSpacecraft->getDirection();
		Vector3 desiredVelocity = direction * std::max(0.001f, mMove) * Spacecraft::MAX_SPEED;
		Vector3 currentVelocity = mSpacecraft->getLinearVelocity();
		DebugDisplay::getSingleton().drawLine(mSpacecraft->getPosition(), mSpacecraft->getPosition() + desiredVelocity * 10.0f, ColourValue(0.0f, 1.0f, 0.0f));

		float speed = mSpacecraft->getLinearVelocity().length();
		float strength = Math::Clamp(1.0f - speed / Spacecraft::MAX_SPEED, 0.0f, 1.0f) / 0.75f + 0.25f;
		float steer = Math::Clamp((float)mSteer, -strength, strength);
		desiredVelocity = Quaternion(Ogre::Radian(Math::PI * steer * 0.25f), Ogre::Vector3(0.0f, 1.0f, 0.0f)) * desiredVelocity;

		mSpacecraft->setSteeringCommand((desiredVelocity - currentVelocity) + wallAvoidance());
	}
	
	if (getOnSpot())
	{
		mSpacecraft->heal(delta);
	}
}

bool HumanController::keyPressed(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_A)
	{
		mSteer = 1.0f;
		return true;
	}

	if (arg.key == OIS::KC_D)
	{
		mSteer = -1.0f;
		return true;
	}

	if (arg.key == OIS::KC_W)
	{
		mMove = 1.0f;
		return true;
	}

	if (arg.key == OIS::KC_S)
	{
		mMove = -1.0f;
		return true;
	}

	return false;
}

bool HumanController::keyReleased(const OIS::KeyEvent &arg)
{
	if ((arg.key == OIS::KC_A) && (mSteer > 0.0f))
	{
		mSteer = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_D) && (mSteer < 0.0f))
	{
		mSteer = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_W) && (mMove > 0.0f))
	{
		mMove = 0.0f;
		return true;
	}

	if ((arg.key == OIS::KC_S) && (mMove < 0.0f))
	{
		mMove = 0.0f;
		return true;
	}

	if (arg.key == OIS::KC_SPACE)
	{
		mSpacecraft->shoot();
		return true;
	}

	return false;
}