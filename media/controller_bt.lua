class 'Behavior'

STATUS_INVALID = 0;
STATUS_RUNNING = 1;
STATUS_SUCCESS = 2;
STATUS_FAILURE = 3;
STATUS_NAMES = {[STATUS_INVALID] = "STATUS_INVALID", [STATUS_RUNNING] = "STATUS_RUNNING", [STATUS_SUCCESS] = "STATUS_SUCCESS", [STATUS_FAILURE] = "STATUS_FAILURE"}


function Behavior:__init()
	self.name = "Behavior"
	self.status = STATUS_INVALID
end

function Behavior:onInitialized()

end

function Behavior:onUpdate(delta)
	return STATUS_SUCCESS
end

function Behavior:onTerminated()
	self.currentChild = 1
end

function Behavior:printTree(space)
	log(space .. self.name .. " status: " .. STATUS_NAMES[self.status])
end

function Behavior:reset()
	status = STATUS_INVALID
end


function Behavior:tick(delta)
	if self.status == STATUS_INVALID then
		self:onInitialized()
	end

	self.status = self:onUpdate(delta)
	--log(self.name .. " " .. STATUS_NAMES[self.status])
	if self.status ~= STATUS_RUNNING then
		self:onTerminated()
	end
	
	return self.status
end

class 'Composite' (Behavior)

function Composite:__init() super()
	self.name = "Composite"
	self.children = {}
end

function Composite:add(child)
	table.insert(self.children, child);
end

function Composite:printTree(space)
	Behavior.printTree(self, space)
	
	for k,v in pairs(self.children) do
		v:printTree(space .. " ")
	end
end


class 'Sequence' (Composite)

function Sequence:__init() super()
	self.name = "Sequence"
end

function Sequence:onInitialized()
	self.currentChild = 1
end

function Sequence:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);
		
		if s ~= STATUS_SUCCESS then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_SUCCESS
		end
	end
	
	return STATUS_INVALID
end

class 'Selector' (Composite)

function Selector:__init() super()
	self.name = "Selector"
end

function Selector:onInitialized()
	self.currentChild = 1
end

function Selector:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		local s = child:tick(delta);
		if s ~= STATUS_FAILURE then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_FAILURE
		end
	end
	
	return STATUS_INVALID
end


class 'Action' (Behavior)

function Action:__init(controller) super()
	self.controller = controller
	self.name = "Action"
	
end

function Action:onUpdate(delta)
	--return STATUS_RUNNING
end

class 'Condition' (Behavior)

function Condition:__init(controller) super()
	self.controller = controller
	self.name = "Condition"
end

function Condition:testCondition()
	return false
end
	
function Condition:onUpdate()
	if self:testCondition() then
		return STATUS_SUCCESS
	else
		return STATUS_FAILURE
	end
end

-- -----------------------------------CONDITIONS--------------------------------------

class 'RechargeFlagSetCondition' (Condition)

function RechargeFlagSetCondition:__init(controller) super(controller)
	self.name = "RechargeFlagSetCondition"
end

function RechargeFlagSetCondition:testCondition()
	return self.controller:getRechargeFlag()
end


class 'OnSpotCondition' (Condition)

function OnSpotCondition:__init(controller) super(controller)
	self.name = "OnSpotCondition"
end

function OnSpotCondition:testCondition()
	return self.controller:getOnSpot()
end


class 'EnemyVisCondition' (Condition)
function EnemyVisCondition:__init(controller) super(controller)
	self.name = "EnemyVisCondition"
end

function EnemyVisCondition:testCondition()
	return self.controller:getEnemyVis()
end


class 'AmmoHpLowOrEmpty' (Condition)
function AmmoHpLowOrEmpty:__init(controller) super(controller)
	self.name = "AmmoHpLowOrEmpty"
end

function AmmoHpLowOrEmpty:testCondition()
	return (self.controller:getAmmoOfShip() > 0 and self.controller:getHpOfShip() > 0.15)
end


class 'AmmoHpStillOkOrRecharge' (Condition)
function AmmoHpStillOkOrRecharge:__init(controller) super(controller)
	self.name = "AmmoHpStillOkOrRecharge"
end

function AmmoHpStillOkOrRecharge:testCondition()
	return  (self.controller:getAmmoOfShip() > 5 and self.controller:getHpOfShip() > 0.6)
end


class 'WasEnemyVisCondition' (Condition)
function WasEnemyVisCondition:__init(controller) super(controller)
	self.name = "WasEnemyVisCondition"
end

function WasEnemyVisCondition:testCondition()
	return self.controller:wasEnemyVis()
end

-- ------------------------------------ACTIONS------------------------------------------

class 'FlyToBaseAction' (Action)

function FlyToBaseAction:__init(controller) super(controller)
	self.name = "FlyToBaseAction"
end

function FlyToBaseAction:onUpdate(delta)
	local fin = self.controller:flyToBase()

	if fin then
		return STATUS_SUCCESS
	else
		return STATUS_RUNNING
	end
end


class 'HealAction' (Action)
function HealAction:__init(controller) super(controller)
	self.name = "HealAction"
end

function HealAction:onUpdate(delta)
	local fin = self.controller:heal()
	if fin then
		self.controller:setRechargeFlag(false)
	--end
	--return STATUS_SUCCESS
	else
		return STATUS_RUNNING
	end
end


class 'ChaseAttackAction' (Action)
function ChaseAttackAction:__init(controller) super(controller)
	self.name = "ChaseAttackAction"
end

function ChaseAttackAction:onUpdate(delta)
	self.controller:chaseAndAttackEnemy()
	return STATUS_SUCCESS
end


class 'SetRechargeAction' (Action)
function SetRechargeAction:__init(controller) super(controller)
	self.name = "SetRechargeAction"
end

function SetRechargeAction:onUpdate(delta)
	self.controller:setRechargeFlag(true)
	return STATUS_SUCCESS
end

class 'SeekAction' (Action)
function SeekAction:__init(controller) super(controller)
	self.name = "SeekAction"
end

function SeekAction:onUpdate(delta)
	self.controller:seekEnemy()
	return STATUS_SUCCESS
end


class "PatrolAction" (Action)

function PatrolAction:__init(controller) super(controller)
	self.name = "PatrolAction"
end

function PatrolAction:onUpdate(delta)
	self.controller:patrol()
	return STATUS_SUCCESS
end

-- --------------------------------------------------------------------------------------

init = function(controller)
	local root = Selector()
	root.name = "rooSelector"

	local rechargeFlagSequ = Sequence()
	rechargeFlagSequ.name = "rechargeFlagSelector"
	local rechargeFlagCon = RechargeFlagSetCondition(controller)
	local onSpotSele = Selector()
	onSpotSele.name = "onSpotSelector"
	local onSpotSequ = Sequence()
	onSpotSequ.name = "onSpotSequence"
	local onSpotCon = OnSpotCondition(controller)
	local heal = HealAction(controller)
	local flyToBase = FlyToBaseAction(controller)
	
	local enemyVisSequ = Sequence()
	enemyVisSequ.name = "enemyVisSequence"
	local enemyVisCon = EnemyVisCondition(controller)
	local ammoHpOkSele = Selector()
	ammoHpOkSele.name = "ammoHpOkSelector"
	local ammoHpOkSequ = Sequence()
	ammoHpOkSequ.name = "ammoHpOkSequence"
	local ammoHpOkCon = AmmoHpLowOrEmpty(controller)
	local chaseAttack = ChaseAttackAction(controller)
	local setRecharge = SetRechargeAction(controller)

	local wasEnemyVisSele = Selector()
	wasEnemyVisSele.name = "wasEnemyVisSelector"
	
	local wasEnemyVisSequ = Sequence()
	wasEnemyVisSequ.name = "wasEnemyVisSequence"
	local wasEnemyVisCon = WasEnemyVisCondition(controller)
	local ammoHpOkSele2 = Selector()
	ammoHpOkSele2.name = "ammoHpOkSelector2"
	local ammoHpOkSequ2 = Sequence()
	ammoHpOkSequ2.name = "ammoHpOkSequence2"
	local ammoHpOkCon2 = AmmoHpLowOrEmpty(controller)
	local seek = SeekAction(controller)
	local setRecharge2 = SetRechargeAction(controller)

	local ammoHpOkSele3 = Selector()
	ammoHpOkSele3.name = "ammoHpOkSelector3"
	local ammoHpOkSequ3 = Sequence()
	ammoHpOkSequ3.name = "ammoHpOkSequence3"
	local ammoHpOkCon3 = AmmoHpStillOkOrRecharge(controller)
	local patrol = PatrolAction(controller)
	local setRecharge3 = SetRechargeAction(controller)

	root.add(root, rechargeFlagSequ)
		root.add(rechargeFlagSequ, rechargeFlagCon)
		root.add(rechargeFlagSequ, onSpotSele)
			root.add(onSpotSele, onSpotSequ)
				root.add(onSpotSequ, onSpotCon)
				root.add(onSpotSequ, heal)
			root.add(onSpotSele, flyToBase)
	root.add(root, enemyVisSequ)
		root.add(enemyVisSequ, enemyVisCon)
		root.add(enemyVisSequ, ammoHpOkSele)
			root.add(ammoHpOkSele, ammoHpOkSequ)
				root.add(ammoHpOkSequ, ammoHpOkCon)
				root.add(ammoHpOkSequ, chaseAttack)
			root.add(ammoHpOkSele, setRecharge)
	root.add(root, wasEnemyVisSele)
		root.add(wasEnemyVisSele, wasEnemyVisSequ)
			root.add(wasEnemyVisSequ, wasEnemyVisCon)
			root.add(wasEnemyVisSequ, ammoHpOkSele2)
				root.add(ammoHpOkSele2, ammoHpOkSequ2)
					root.add(ammoHpOkSequ2, ammoHpOkCon2)
					root.add(ammoHpOkSequ2, seek)
				root.add(ammoHpOkSele2, setRecharge2)
		root.add(wasEnemyVisSele, ammoHpOkSele3)
			root.add(ammoHpOkSele3, ammoHpOkSequ3)
				root.add(ammoHpOkSequ3, ammoHpOkCon3)
				root.add(ammoHpOkSequ3, patrol)
			root.add(ammoHpOkSele3, setRecharge3)
	return root;
end


update = function(controller)
end


