function MapUpdate(pos,yaw, camera)
    camera.move(camera, pos)
    local q = Math.Quaternion(Math.Matrix3())
    local q2 = Math.Quaternion(Math.Matrix3())
    q.FromAngleAxis(q, Math.Radian(Math.Degree(-90)), Math.Vector3(1,0,0))
    q2.FromAngleAxis(q2, Math.Radian(yaw)-Math.Radian(Math.Degree(180)), Math.Vector3(0,1,0))
    camera.setOrientation(camera, q2*q)
end