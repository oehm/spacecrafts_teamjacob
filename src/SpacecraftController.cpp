#include "StdAfx.h"

#include "SpacecraftController.h"
#include "DebugDisplay.h"
#include "GameConfig.h"
#include "Path.h"
#include "MathUtil.h"
#include "WorldUtil.h"

using namespace Ogre;

static ColourValue DEBUG_COLOUR_WALL_AVOIDANCE(1.0f, 0.0f, 0.0f);
static ColourValue DEBUG_COLOUR_OBSTALCE_AVOIDANCE(1.0f, 0.0f, 1.0f);


SpacecraftController::SpacecraftController(Spacecraft* spacecraft, CommandCenter* c1) :
	mSpacecraft(spacecraft),
	mCommandcenter(c1)
{
}


SpacecraftController::~SpacecraftController()
{}


bool SpacecraftController::getOnSpot()
{
	NavigationNode* mPos = NavigationGraph::getSingletonPtr()->getNodeAt(mSpacecraft->getPosition());
	for (int i = 0; i < 4; i++)
	{
		if (mCommandcenter->getChargeSpots()[i] == mPos){
			return true;
		}
	}
	return false;
} 

Ogre::Vector3 SpacecraftController::seek(const Ogre::Vector3& target) const
{
	Ogre::Vector3 desiredVelocity = target - mSpacecraft->getPosition();
	desiredVelocity.normalise();
	desiredVelocity *= Spacecraft::MAX_SPEED;

	return desiredVelocity - mSpacecraft->getLinearVelocity();
}

Ogre::Vector3 SpacecraftController::arrive(const Ogre::Vector3& target) const
{
	DebugDisplay::getSingleton().drawCircle(target, 1.0f, 16, ColourValue::Red);

	Ogre::Vector3 toTarget = target - mSpacecraft->getPosition();
	float dist = toTarget.length();

	if (dist > 0)
	{
		const float DECELERATION_TWEAKER = GameConfig::getSingleton().getValueAsReal("Steering/DecelerationTweaker");
		float speed = dist / DECELERATION_TWEAKER;

		speed = std::min(speed, Spacecraft::MAX_SPEED);
		Ogre::Vector3 desiredVelocity = toTarget * speed / dist;

		return desiredVelocity - mSpacecraft->getLinearVelocity();
	}

	return Ogre::Vector3(0.0f);
}

Ogre::Vector3 SpacecraftController::followPath(const Path& path, float& currentParam) const
{
	if (path.isEmpty())
	{
		//assert();

		return Vector3(0, 0, 0);
	}


	//currentParam = path.getParam(mSpacecraft->getPosition(), currentParam);
	currentParam = path.getParam(mSpacecraft->getPosition(), currentParam);

	float targetParam = currentParam + GameConfig::getSingleton().getValueAsReal("Steering/PathFollowingTargetOffset");

	Vector3 currentPosition = path.getPosition(currentParam);
	Vector3 targetPosition = path.getPosition(targetParam);

	DebugDisplay::getSingleton().drawCircle(currentPosition, 2, 8, ColourValue::Black);
	DebugDisplay::getSingleton().drawCircle(targetPosition, 2, 8, ColourValue::Red);

	if (path.isPathEnd(targetParam))
	{
		return arrive(targetPosition);
	}

	return seek(targetPosition);
}

Ogre::Vector3 SpacecraftController::wallAvoidance() const
{		
	Ogre::Vector3 myPos = mSpacecraft->getPosition();
	Ogre::Vector3 myFuturePos = mSpacecraft->getPosition() + mSpacecraft->getLinearVelocity();
	Ogre::Vector3 collisonPos, collisonNorm;

	if (WorldUtil::rayCast(myPos, myFuturePos, collisonPos, collisonNorm))
	{
		Ogre::Vector3 dir = collisonPos + collisonNorm;
		return -dir;
	}
	return Vector3(0.0f, 0.0f, 0.0f);
}

Ogre::Vector3 SpacecraftController::obstacleAvoidance() const
{
	const std::vector<Spacecraft*> spacecrafts = WorldUtil::getAllSpacecrafts();

	for (const Spacecraft* obstacle : spacecrafts)
	{
		if (obstacle == mSpacecraft)
		{
			continue;
		}

		float t0, t1;

		if (MathUtil::sphereSweepTest(mSpacecraft->getRadius(), mSpacecraft->getPosition(), mSpacecraft->getLinearVelocity(),
			obstacle->getRadius(), obstacle->getPosition(), obstacle->getLinearVelocity(), t0, t1))
		{
			if (t0 >= 0.0f && t0 < 5.0f)
			{
				Vector3 myPos = mSpacecraft->getPosition() + mSpacecraft->getLinearVelocity() * t0;
				Vector3 obstaclePos = obstacle->getPosition() + obstacle->getLinearVelocity() * t0;

				Vector3 dir = obstaclePos - myPos;

				dir.normalise();

				return -dir * Spacecraft::MAX_SPEED;
			}
		}
	}

	return Ogre::Vector3(0.0f);
}