#include "stdafx.h"


#include "OgreBulletDynamicsRigidBody.h"				 // for OgreBullet
#include "Shapes/OgreBulletCollisionsStaticPlaneShape.h" // for static planes
#include "Shapes/OgreBulletCollisionsBoxShape.h"		 // for Boxes
#include "Shapes/OgreBulletCollisionsSphereShape.h"
#include "Shapes/OgreBulletCollisionsCylinderShape.h"
#include "DebugOverlay.h"
#include "DebugDisplay.h"
#include "GameApplication.h"
#include "GameConfig.h"

#include "OgreConsole.h"
#include "Spacecraft.h"
#include "SpacecraftController.h"
#include "HumanController.h"
#include "AIController.h"
#include "Rocket.h"
#include "CommandCenter.h"
#include "ICollider.h"
#include "WorldUtil.h"

#include "LuaScriptManager.h"
#include "NavigationGraph.h"
#include "NavigationNode.h"
#include "WorldUtil.h"

#include "CompositorListenerHitEffect.h"
#include "CompositorListenerSpeedEffect.h"

template<> GameApplication* Ogre::Singleton<GameApplication>::msSingleton = 0;
GameApplication* GameApplication::getSingletonPtr(void)
{
	return msSingleton;
}
GameApplication& GameApplication::getSingleton(void)
{
	assert(msSingleton);
	return *msSingleton;
}

GameApplication::GameApplication(Mode mode, String address) :
mHumanController(NULL),
mRocketCounter(0),
mWorld(NULL),
mDebugDrawer(NULL),
mDebugOverlay(NULL),
mScriptingManager(NULL),
mShowDebugDraw(false),
mShowNavigationGraph(false),
mFollowPlayerCam(true),
mSynchTimer(0.0f),
mMode(mode),
mAddress(address),
mCompListenerHitEffect(NULL),
mCompListenerSpeedEffect(NULL),
mIsGameWon(false)
{}

GameApplication::~GameApplication()
{
	delete mCompListenerHitEffect;
	delete mCompListenerSpeedEffect;

	for (size_t i = 0; i < mControllers.size(); i++)
	{
		delete mControllers[i];
	}

	for (size_t i = 0; i < mSpacecrafts.size(); i++)
	{
		delete mSpacecrafts[i];
	}

	for (size_t i = 0; i < mObjectives.size(); i++)
	{
		delete mObjectives[i];
	}

	std::list<Rocket*>::iterator it;

	for (it = mRockets.begin(); it != mRockets.end(); ++it)
	{
		delete *it;
	}

	mReleasedRockets.unique();
	for (it = mReleasedRockets.begin(); it != mReleasedRockets.end(); ++it)
	{
		mRockets.remove(*it);
		delete *it;
	}

	mControllers.clear();
	mSpacecrafts.clear();
	mRockets.clear();
	mReleasedRockets.clear();

	SAFE_DELETE(mDebugDrawer);

	SAFE_DELETE(mWorld);

}

void GameApplication::createScene(void)
{
	// Set ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));

	// Create a light
	Light* l = mSceneMgr->createLight("MainLight");
	l->setPosition(0, 1000, 0);
	mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);

	new DebugDisplay(mSceneMgr, 0.5f);
	DebugDisplay::getSingleton().setEnabled(false);

	mDebugOverlay = new DebugOverlay();

	new OgreConsole;
	OgreConsole::getSingleton().init(mRoot);
	OgreConsole::getSingleton().setVisible(false);

	createDynamicWorld(Vector3(0.0f, -9.81f, 0.0f), AxisAlignedBox(Ogre::Vector3(-10000, -10000, -10000), Ogre::Vector3(10000, 10000, 10000)));
	createWalls();

	//skybox
	mSceneMgr->setSkyDome(true, "skyBox", 5, 8);

	// now create navigation graph
	new NavigationGraph(mSceneMgr, -280, -600, 1600, 800);
	NavigationGraph::getSingleton().calcGraph(mWorld);
	NavigationGraph::getSingleton().setDebugDisplayEnabled(mShowNavigationGraph);

	setScriptSettings();

	createSpacecrafts();
	commandcenterEnemy->createChareSpots();
	commandcenterPlayer->createChareSpots();
	setCompositors();
}

void GameApplication::setScriptSettings()
{
	mScriptingManager = new scripting::Manager();
	mScriptingManager->runScriptFile("../../scripts/MapMovement.lua");
	mScriptingManager->runScriptFile("../../scripts/MapPoints.lua");
	mScriptingManager->runScriptFile("../../media/controller_bt.lua");
	fileWatcher.addWatch("..\\..\\scripts", new UpdateListener(mScriptingManager));
}


void GameApplication::setCompositors()
{
	Ogre::CompositorInstance* CompositorRadialBlur = Ogre::CompositorManager::getSingleton().addCompositor(mMainViewport, "SpeedEffect");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mMainViewport, "SpeedEffect", true);

	Ogre::CompositorInstance* CompositorHitEffect = Ogre::CompositorManager::getSingleton().addCompositor(mMainViewport, "HitEffect");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mMainViewport, "HitEffect", false);

	Ogre::CompositorManager::getSingleton().addCompositor(mViewportMM, "MiniMap");
	Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewportMM, "MiniMap", true);
	mCamera2->setAspectRatio(Ogre::Real(mMainViewport->getActualWidth()) / Ogre::Real(mMainViewport->getActualHeight()));

	mCompListenerHitEffect = new CompositorListenerHitEffect();
	CompositorHitEffect->addListener(mCompListenerHitEffect);

	mCompListenerSpeedEffect = new CompositorListenerSpeedEffect();
	CompositorRadialBlur->addListener(mCompListenerSpeedEffect);
}

void GameApplication::createDynamicWorld(Vector3 &gravityVector, AxisAlignedBox &bounds)
{
	// Start Bullet
	mWorld = new OgreBulletDynamics::DynamicsWorld(mSceneMgr, bounds, gravityVector);

	// add Debug info display tool
	mDebugDrawer = new OgreBulletCollisions::DebugDrawer();
	mDebugDrawer->setDrawWireframe(true);	// we want to see the Bullet containers

	mWorld->setDebugDrawer(mDebugDrawer);
	mWorld->setShowDebugShapes(false);		// enable it if you want to see the Bullet containers
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("debugDrawer", Ogre::Vector3::ZERO);
	node->attachObject(static_cast <SimpleRenderable*> (mDebugDrawer));

	// Define a floor plane mesh
	Entity *ent;
	Plane p;
	p.normal = Vector3(0, 1, 0); p.d = 0;
	MeshManager::getSingleton().createPlane("FloorPlane",
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		p, 200000, 200000, 20, 20, true, 1, 9000, 9000,
		Vector3::UNIT_Z);
	// Create an entity (the floor)
	ent = mSceneMgr->createEntity("floor", "FloorPlane");
	ent->setMaterialName("ground");
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);

	Ogre::SceneNode* SceneNodeWorld = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	SceneNodeWorld = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	// add collision detection to it
	OgreBulletCollisions::CollisionShape *Shape;
	Shape = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0, 1, 0), 0);
	// a body is needed for the shape
	OgreBulletDynamics::RigidBody *defaultPlaneBody = new OgreBulletDynamics::RigidBody("BasePlane", mWorld, WorldUtil::FILTER_STATIC_GROUND, WorldUtil::FILTER_ALL);

	defaultPlaneBody->setStaticShape(Shape, 0.0f, 0.8f); // (shape, restitution, friction)
}

void GameApplication::createWalls()
{
	commandcenterEnemy = new CommandCenter(mSceneMgr, mWorld, Vector3(0.0f, 0.0f, -200.0f), "red"); //color kann man ggf. �ber die ID setzen?
	commandcenterPlayer = new CommandCenter(mSceneMgr, mWorld, Vector3(1000.0f, 0.0f, -200.0f), "green");

	Wall* wall;
	//base1
	wall = new Wall(mSceneMgr, mWorld, Vector3(100.0f, 5.0f, -100.0f), Vector3(100.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(100.0f, 5.0f, -220.0f), Vector3(100.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -100.0f), Vector3(-100.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -220.0f), Vector3(-100.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -100.0f), Vector3(-20.0f, 5.0f, -100.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(20.0f, 5.0f, -100.0f), Vector3(100.0f, 5.0f, -100.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-100.0f, 5.0f, -300.0f), Vector3(-20.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(20.0f, 5.0f, -300.0f), Vector3(100.0f, 5.0f, -300.0f));
	//base2
	wall = new Wall(mSceneMgr, mWorld, Vector3(1100.0f, 5.0f, -100.0f), Vector3(1100.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(1100.0f, 5.0f, -220.0f), Vector3(1100.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(900.0f, 5.0f, -100.0f), Vector3(900.0f, 5.0f, -180.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(900.0f, 5.0f, -220.0f), Vector3(900.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(900.0f, 5.0f, -100.0f), Vector3(980.0f, 5.0f, -100.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(1020.0f, 5.0f, -100.0f), Vector3(1100.0f, 5.0f, -100.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(900.0f, 5.0f, -300.0f), Vector3(980.0f, 5.0f, -300.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(1020.0f, 5.0f, -300.0f), Vector3(1100.0f, 5.0f, -300.0f));
	//outwalls
	wall = new Wall(mSceneMgr, mWorld, Vector3(-300.0f, 5.0f, 200.0f), Vector3(-300.0f, 5.0f, -600.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-300.0f, 5.0f, 200.0f), Vector3(1300.0f, 5.0f, 200.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(1300.0f, 5.0f, 200.0f), Vector3(1300.0f, 5.0f, -600.0f));
	wall = new Wall(mSceneMgr, mWorld, Vector3(-300.0f, 5.0f, -600.0f), Vector3(1300.0f, 5.0f, -600.0f));
	//midwall
	wall = new Wall(mSceneMgr, mWorld, Vector3(500.0f, 5.0f, -140.0f), Vector3(500.0f, 5.0f, -260.0f));
}


void GameApplication::createSpacecrafts()
{
	Spacecraft* craft1 = createSpacecraft(Vector3(950.0f, 10.0f, -200.0f), "green");//new Spacecraft(0, "craft1", mSceneMgr, mWorld, Vector3(950.0f, 10.0f, -200.0f), "green", 36);

	Spacecraft* craft2 = createSpacecraft(Vector3(-50.0f, 10.0f, -150.0f), "red");//new Spacecraft(1, "craft2", mSceneMgr, mWorld, Vector3(-50.0f, 10.0f, -150.0f), "red");
	Spacecraft* craft3 = createSpacecraft(Vector3(50.0f, 10.0f, -250.0f), "red");//new Spacecraft(2, "craft3", mSceneMgr, mWorld, Vector3(50.0f, 10.0f, -250.0f), "red");


	mObjectives.push_back(new ObjectiveKillCraft(craft2));
	mObjectives.push_back(new ObjectiveKillCraft(craft3));

	// create controllers
	mHumanController = new HumanController(craft1, commandcenterPlayer);
	mControllers.push_back(mHumanController);

	static const Ogre::Vector3 TARGETS[] = {
		Ogre::Vector3(50.0f, 5.0f, -150.0f),
		Ogre::Vector3(50.0f, 5.0f, -250.0f),
		Ogre::Vector3(-50.0f, 5.0f, -250.0f),
		Ogre::Vector3(-50.0f, 5.0f, -150.0f)
	};

	std::vector<Ogre::Vector3> waypoints1;
	waypoints1.push_back(TARGETS[0]);
	waypoints1.push_back(TARGETS[1]);
	waypoints1.push_back(TARGETS[2]);
	waypoints1.push_back(TARGETS[3]);

	std::vector<Ogre::Vector3> waypoints2;
	waypoints2.push_back(TARGETS[3]);
	waypoints2.push_back(TARGETS[2]);
	waypoints2.push_back(TARGETS[1]);
	waypoints2.push_back(TARGETS[0]);


	AIController* aiController1 = new AIController(craft2, craft1, commandcenterEnemy, Path(waypoints1, Path::PATH_LOOP));
	mControllers.push_back(aiController1);
	AIController* aiController2 = new AIController(craft3, craft1, commandcenterEnemy, Path(waypoints2, Path::PATH_LOOP));
	mControllers.push_back(aiController2);
}


Spacecraft* GameApplication::createSpacecraft(const Vector3& position, const Ogre::String& texture)
{
	Ogre::String name = "craft_";

	std::stringstream ss;
	ss << mSpacecrafts.size();
	name += ss.str();

	name += "_" + texture;

	Spacecraft* craft = new Spacecraft(mSpacecrafts.size(), name, mSceneMgr, mWorld, position, texture, 36);
	craft->setOrientation(Quaternion(Radian(Math::PI), Vector3(0.0f, 1.0f, 0.0f)));
	craft->setOrientation(Ogre::Quaternion(Radian(Degree(-90)), Ogre::Vector3(0, 1, 0)));

	mSpacecrafts.push_back(craft);

	return craft;
}

void GameApplication::deleteHumanSpacecraft(Spacecraft* toDelete, Spacecraft* newOne)
{
	for (std::vector<Spacecraft*>::iterator it = mSpacecrafts.begin(); it < mSpacecrafts.end();)
	{
		if (*it == toDelete){
			mSceneMgr->destroyEntity(toDelete->getMName());
			delete *it;
			it = mSpacecrafts.erase(it);
		}
		else
		{
			++it;
		}
	}
	//ugly but needed
	for (int i = 1; i < mControllers.size(); i++)
	{
		AIController* cont = (AIController*)mControllers[i];
		cont->setHumanSpacecraft(newOne);
	}
}

void GameApplication::createCamera(void)
{
	BaseApplication::createCamera();
	mCamera->setPosition(Ogre::Vector3(0, 50, 100));
	setDragLook(mGameConfig->getValueAsBool("Game/DragLook"));
}

bool GameApplication::frameStarted(const Ogre::FrameEvent& evt)
{
	/*if (mScriptingManager->hasGlobalFunction("changePosition"))
	{
	mScriptingManager->GetGlobalTable()["changePosition"](mPlayerSquere, mSpacecrafts[0]->getPosition().x, 200, mSpacecrafts[0]->getPosition().z);
	}*/
	if (mScriptingManager->hasGlobalFunction("MapUpdate"))
	{
		mScriptingManager->GetGlobalTable()["MapUpdate"](mHumanController->getSpacecraft()->getPosition(), mHumanController->getSpacecraft()->getYaw(), mCamera2);
	}
	//nur testweise in dieser funktion

	bool ret = BaseApplication::frameStarted(evt);

	float delta = evt.timeSinceLastFrame;
	update(delta);

	// Right before the frame is rendered, call DebugDrawer::build().
	DebugDisplay::getSingleton().build();
	return ret;
}

void GameApplication::update(float delta)
{
	//update filewatcher
	fileWatcher.update();

	// update controllers
	for (size_t i = 0; i < mControllers.size(); i++)
	{
		mControllers[i]->update(delta);
	}

	//send spacecraft flying if won
	if (mIsGameWon)
	{
		mHumanController->getSpacecraft()->setSteeringCommand(Vector3(0.0f, 100.0f, 10.0f), 0.0f);
	}

	// update spacecrafts
	for (size_t i = 0; i < mSpacecrafts.size(); i++)
	{
		mSpacecrafts[i]->update(delta);
	}

	// update rockets
	for (std::list<Rocket*>::iterator it = mRockets.begin(); it != mRockets.end(); it++)
	{
		(*it)->update(delta);
	}

	//check for win
	mIsGameWon = true;
	for (size_t i = 0; i < mObjectives.size(); i++)
	{
		if (!mObjectives[i]->isCompleted())
		{
			mIsGameWon = false;
			break;
		}
	}


	mCamera2->setPosition(Vector3(mHumanController->getSpacecraft()->getPosition().x, 70, mHumanController->getSpacecraft()->getPosition().z));
	Ogre::Vector3 direction = mHumanController->getSpacecraft()->getDirection();
	float angle = tan(direction.z / direction.x);

	// update Bullet Physics animation
	mWorld->stepSimulation(delta, 10);

	// call onCollision of colliders
	btDynamicsWorld* dynamicWorld = mWorld->getBulletDynamicsWorld();

	int numManifolds = dynamicWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; i++)
	{
		btPersistentManifold* contactManifold = dynamicWorld->getDispatcher()->getManifoldByIndexInternal(i);
		const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
		const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());

		int numContacts = contactManifold->getNumContacts();
		for (int j = 0; j < numContacts; j++)
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance() < 0.f)
			{
				const btVector3& ptA = pt.getPositionWorldOnA();
				const btVector3& ptB = pt.getPositionWorldOnB();
				const btVector3& normalOnB = pt.m_normalWorldOnB;

				void* pointer0 = contactManifold->getBody0()->getUserPointer();
				void* pointer1 = contactManifold->getBody1()->getUserPointer();
				ICollider* collider0 = (pointer0 != NULL) ? static_cast<ICollider*>(pointer0) : NULL;
				ICollider* collider1 = (pointer1 != NULL) ? static_cast<ICollider*>(pointer1) : NULL;

				if (collider0 != NULL)
				{
					collider0->onCollision(collider1);
				}

				if (collider1 != NULL)
				{
					collider1->onCollision(collider0);
				}
			}
		}
	}

	// delete rockets if neccessary
	if (mReleasedRockets.size() > 0)
	{
		mReleasedRockets.unique();
		std::list<Rocket*>::iterator it;

		for (it = mReleasedRockets.begin(); it != mReleasedRockets.end(); ++it)
		{
			mRockets.remove(*it);
			delete *it;
		}

		mReleasedRockets.clear();
	}

	if (mHumanController != NULL)
	{
		Spacecraft* craft = mHumanController->getSpacecraft();

		if (mFollowPlayerCam)
		{
			// look at human craft.
			if (!mIsGameWon)
			{
				mCamera->setPosition(craft->getPosition() - craft->getDirection() * Vector3(35.0f, 0.0f, 35.0f)
					+ (craft->getDirection().crossProduct(Vector3(0.0f, 1.0f, 0.0f))) * craft->getAngularVelocity() * 3
					+ Vector3(0.0f, 10.0f, 0.0f) * Spacecraft::MAX_SPEED / (craft->getLinearVelocity().length() + Spacecraft::MAX_SPEED));
			}
			mCamera->lookAt(craft->getPosition());
		}
	}

	//calc inverse rotation mat
	Vector3 pos, scale;
	Quaternion rota;
	mCamera->getViewMatrix().decomposition(pos, scale, rota);
	Matrix4 matInvsRota = rota.Inverse();
	//and pass it to the skybox fragment shader
	Ogre::MaterialManager::getSingletonPtr()->getByName("skyBox").staticCast<Ogre::Material>()
		->getTechnique(0)->getPass(0)->getFragmentProgramParameters()->setNamedConstant("matInvsRota", matInvsRota);  //ogre syntax ftw

	//Set Hit Effect, if health is <= 20%
	if (!mIsGameWon)
	{
		Ogre::CompositorManager::getSingleton().setCompositorEnabled(mMainViewport, "HitEffect", mHumanController->getSpacecraft()->getHealth() <= 0.20);
	}

	//Speed-effect
	if (mCompListenerSpeedEffect && (Spacecraft::MAX_SPEED > 0))
		mCompListenerSpeedEffect->setValue(mHumanController->getSpacecraft()->getLinearVelocity().length() / Spacecraft::MAX_SPEED);
}

bool GameApplication::frameRenderingQueued(const FrameEvent& evt)
{
	bool ret = BaseApplication::frameRenderingQueued(evt);
	return ret;
}

bool GameApplication::frameEnded(const Ogre::FrameEvent& evt)
{
	bool ret = BaseApplication::frameEnded(evt);
	// After the frame is rendered, call DebugDrawer::clear()
	DebugDisplay::getSingleton().clear();

	return ret;
}

bool GameApplication::keyPressed(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_H)  // hit yourself (testing)
	{
		mHumanController->getSpacecraft()->hit();
	}

	if ((mHumanController != NULL) && mHumanController->keyPressed(arg))
	{
		return true;
	}

	return BaseApplication::keyPressed(arg);
}

bool GameApplication::keyReleased(const OIS::KeyEvent &arg)
{
	if ((mHumanController != NULL) && mHumanController->keyReleased(arg))
	{
		return true;
	}

	if (arg.key == OIS::KC_1)
	{
		mShowDebugDraw = !mShowDebugDraw;
		mWorld->setShowDebugShapes(mShowDebugDraw);
		DebugDisplay::getSingleton().setEnabled(mShowDebugDraw);
	}

	if (arg.key == OIS::KC_2)
	{
		mShowNavigationGraph = !mShowNavigationGraph;
		NavigationGraph::getSingleton().setDebugDisplayEnabled(mShowNavigationGraph);
	}

	if (arg.key == OIS::KC_3)
	{
		OgreConsole& console = OgreConsole::getSingleton();
		console.setVisible(!console.isVisible());
	}

	if (arg.key == OIS::KC_4)
	{
		mScriptingManager->runScriptFile("../../media/controller.lua");
	}

	if (arg.key == OIS::KC_TAB)
	{
		mFollowPlayerCam = !mFollowPlayerCam;
	}

	return BaseApplication::keyReleased(arg);
}

void GameApplication::createRocket(const Vector3& position, const Vector3& direction, const Ogre::String& texture)
{
	if ((int)mRockets.size() >= mGameConfig->getValueAsInt("Rocket/MaxRockets"))
	{
		Rocket* oldestRocket = *mRockets.begin();
		releaseRocket(oldestRocket);
	}

	Ogre::String name = "Rocket_";
	std::stringstream ss;
	ss << (mRocketCounter++);
	name += ss.str();
	Rocket* rocket = new Rocket(name, mSceneMgr, mWorld, position, direction, texture);
	mRockets.push_back(rocket);
}

void GameApplication::releaseRocket(Rocket* rocket)
{
	mReleasedRockets.push_back(rocket);
}

bool GameApplication::configure(void)
{
	mGameConfig = new GameConfig();

	bool hasConfig = false;

	if (!mGameConfig->getValueAsBool("Game/ShowConfigDialog"))
	{
		hasConfig = mRoot->restoreConfig();
	}

	if (!hasConfig)
	{
		hasConfig = mRoot->showConfigDialog();
	}

	if (hasConfig)
	{
		// If returned true, user clicked OK so initialise
		// Here we choose to let the system create a default rendering window by passing 'true'

		const String titles[] = { "Spacecrafts", "SC-Server", "SC-Client" };
		mWindow = mRoot->initialise(true, titles[mMode]);

		mWindow->setDeactivateOnFocusChange(false);
		// Let's add a nice window icon
		return true;
	}
	else
	{
		return false;
	}
}


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include "shellapi.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	int main(int argc, char* argv[])
		//INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
	{
		LPWSTR* argvw;
		//int argc;
		argvw = CommandLineToArgvW(GetCommandLineW(), &argc);

		GameApplication::Mode mode = GameApplication::MODE_STANDALONE;
		std::string address = "127.0.0.1";

		if (argc > 1)
		{
			if (wcscmp(argvw[1], L"server") == 0)
			{
				mode = GameApplication::MODE_SERVER;
			}
			else if (wcscmp(argvw[1], L"client") == 0)
			{
				mode = GameApplication::MODE_CLIENT;

				if (argc > 2)
				{
					std::wstring ws = std::wstring(argvw[2]);
					address = std::string(ws.begin(), ws.end());
				}
			}
		}

#else
	int main(int argc, char *argv[])
	{
#endif
		// Create application object
		GameApplication app(mode, address);

		try {
			app.go();
		} catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occured: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif