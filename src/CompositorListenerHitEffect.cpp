#include "CompositorListenerHitEffect.h"
#include <OgreTechnique.h>

CompositorListenerHitEffect::CompositorListenerHitEffect() :
	mStep(0.04f),
	mValue(2.0f)
{
}

void CompositorListenerHitEffect::notifyMaterialSetup(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat)
{
	mat->getBestTechnique()->getPass(pass_id)->getFragmentProgramParameters()->setNamedConstant("effect", mValue);
}

void CompositorListenerHitEffect::notifyMaterialRender(Ogre::uint32 pass_id, Ogre::MaterialPtr &mat)
{
	mValue += mStep;
	if ((mValue <= 1.8f) || (mValue >= 3.2f))
		mStep *= -1.0f;
	mat->getBestTechnique()->getPass(pass_id)->getFragmentProgramParameters()->setNamedConstant("effect", mValue);
}
