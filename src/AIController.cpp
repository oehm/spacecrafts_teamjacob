#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"
#include "WorldUtil.h"

#include "CommandCenter.h"

using namespace Ogre;

AIController::AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, CommandCenter* c1, Path& patrol) :
SpacecraftController(spacecraft, c1),
mHumanSpacecraft(mHumanSpacecraft),
mActivePath(NULL),
mLastPath(&patrol),
mCurrentParam(0.0f),
mPatrolPath(patrol),
fieldOfView(70.0f),
enemyOutOfSightTime(9.9f),
maxShootAngle(5.5f),
delta(0.0f),
craftid(spacecraft->getId()),
craftName(spacecraft->getMName()),
mThisCraft(spacecraft)
{

	mRoot = scripting::Manager::getSingleton().callFunction<luabind::object>("init", this);
	//mRoot["printTree"](mRoot, "");

	mActivePath = &mPatrolPath;
	mCurrentParam = mPatrolPath.getParam(spacecraft->getPosition());
	StringStream ss;
	ss << craftid;
	DebugOverlay::getSingleton().addTextBox(ss.str(), "", 0, 60 + (20 * (craftid - 1)), 200, 20, ColourValue(1.0f, 0.0f, 0.0f));
}

void AIController::update(float delta)
{
	if (mActivePath != NULL){
		mActivePath->debugDraw();
		mCurrentParam = mActivePath->getParam(mThisCraft->getPosition());
	}
	this->delta = delta;
	cl = btClock();
	mRoot["tick"](mRoot, delta);
	//mRoot["printTree"](mRoot, "");
	ti = cl.getTimeMilliseconds();
	StringStream ss;
	ss << craftid;
	DebugOverlay::getSingleton().setTextf(ss.str(), "%s UpdateTime: %lu __ STATUS: %s", craftName.c_str(), ti, status.c_str());
	ti = 0l;
}

bool AIController::getEnemyVis() {
	Vector3 enemyDircetion = mHumanSpacecraft->getPosition() - getSpacecraft()->getPosition();
	if (enemyDircetion.length() < 300){
		Ogre::Radian angle = mSpacecraft->getDirection().angleBetween(enemyDircetion);
		if (angle.valueDegrees() > fieldOfView || angle.valueDegrees() < -fieldOfView){
			return false;
		}
		else{
			Vector3  collisionPos(0.0f, 0.0f, 0.0f);
			Vector3 collisionNormal(0.0f, 0.0f, 0.0f);
			bool cast = WorldUtil::rayCast(getSpacecraft()->getPosition(), mHumanSpacecraft->getPosition(), collisionPos, collisionNormal);
			if (!cast){
				enemyOutOfSightTime = 0.0f;
				return true;
			}
		}
	}
	return false;
}

bool AIController::flyToBase()
{
	status = "FlyToBase";
	if (getOnSpot()){
		mActivePath = NULL;
		return true;
	}
	Vector3 nearestSpot;
	float nearestdist = 1000000000.0f;
	for (int i = 0; i < 4; i++)
	{
		float dist = getCommandCenter()->getChargeSpots()[i]->getCenter().distance(getSpacecraft()->getPosition());
		if (dist < nearestdist)
		{
			nearestdist = dist;
			nearestSpot = getCommandCenter()->getChargeSpots()[i]->getCenter();
		}
	}
	float maxDistToSpot = 5.0f;
	if (nearestdist < 15) {
		getSpacecraft()->setSteeringCommand((nearestSpot - getSpacecraft()->getPosition()) + wallAvoidance() + obstacleAvoidance());
	}
	else{
		flyToSpot(nearestSpot, maxDistToSpot);
	}
	return maxDistToSpot > nearestdist;
}

bool AIController::heal()
{
	status = "Heal";
	enemyOutOfSightTime = 99.0f;
	getSpacecraft()->setSteeringCommand(-mThisCraft->getLinearVelocity());
	return getSpacecraft()->heal(delta);
}

void AIController::chaseAndAttackEnemy()
{
	status = "ChaseAndAttackEnemy";
	flyToSpot(mHumanSpacecraft->getPosition(), 30.0f);

	Vector3 enemyDircetion = mHumanSpacecraft->getPosition() - getSpacecraft()->getPosition();
	Ogre::Radian angle = mSpacecraft->getDirection().angleBetween(enemyDircetion);
	if (angle.valueDegrees() > maxShootAngle || angle.valueDegrees() < -maxShootAngle){
		return;
	}
	Vector3  collisionPos, collisionNormal;
	bool cast = WorldUtil::rayCast(getSpacecraft()->getPosition(), mHumanSpacecraft->getPosition(), collisionPos, collisionNormal);
	if (!cast){
		getSpacecraft()->shoot();
		return;
	}
}

void AIController::seekEnemy()
{
	status = "SeekEnemy";
	flyToSpot(mHumanSpacecraft->getPosition(), 30.0f);
	enemyOutOfSightTime += delta;
	return;
}

void AIController::patrol() //not complete
{
	if (flyToPath(&mPatrolPath) > 0){
		status = "SearchPatrolPath";
		return;
	}
	status = "Patrol";
	mActivePath = &mPatrolPath;
	Vector3 linearSteering = followPath(mPatrolPath, mCurrentParam) + obstacleAvoidance() + wallAvoidance();
	getSpacecraft()->setSteeringCommand(linearSteering);
}

float AIController::flyToPath(Path* target)
{
	float maxDistMain = 45.0f;
	float p = getSpacecraft()->getPosition().distance(target->getPosition(target->getParam(mThisCraft->getPosition())));

	if (p < maxDistMain) return 0;

	float targetParam = target->getParam(getSpacecraft()->getPosition());
	Vector3 targetPos = target->getPosition(targetParam);

	return flyToSpot(targetPos, 5.0f);
}

float AIController::flyToSpot(Vector3 target, float maxDist)
{
	bool needNewPath = false;
	float maxDistToPath = 100.0f;

	if (mActivePath == NULL) needNewPath = true;
	else if (mActivePath != mLastPath) needNewPath = true;
	else if (getSpacecraft()->getPosition().distance(mActivePath->getPosition(mCurrentParam)) > maxDistToPath) needNewPath = true;
	else if (getSpacecraft()->getPosition().distance(target) > maxDist) needNewPath = true;

	if (needNewPath)
	{
		if (mActivePath != NULL && mActivePath != &mPatrolPath) delete mActivePath;
		std::vector<Vector3> pPoints = NavigationGraph::getSingletonPtr()->calcPath(getSpacecraft()->getPosition(), target);
		if (pPoints.size() > 0) pPoints.pop_back();//remove last point because otherwise the ship will steer into us!
		if (pPoints.size() < 2){
			getSpacecraft()->setSteeringCommand((getSpacecraft()->getPosition() + getSpacecraft()->getDirection()) + obstacleAvoidance() + wallAvoidance());
			mActivePath = NULL;
			return 100000.0f;
		}
		mActivePath = new Path(pPoints, Path::PATH_NORMAL);
		mLastPath = mActivePath;
	}
	Vector3 steer = followPath(*mActivePath, mCurrentParam) + obstacleAvoidance() + wallAvoidance();
	getSpacecraft()->setSteeringCommand(steer);

	return target.distance(getSpacecraft()->getPosition());
}


