#include "noiseSimplex.cginc"

struct VOut
{
	float4 pos : POSITION0;
	float2 texCoord : TEXCOORD0;
};


VOut skyBox_vs(
	float4 inPos : POSITION,

	uniform float4x4 worldViewProj

)
{
	// Use standardise transform, so work accord with render system specific (RS depth, requires texture flipping, etc)
	VOut output;

	output.pos = mul(worldViewProj, inPos);

	//output.texCoord = float2(atan2(inPos.z, inPos.x), acos(inPos.z / (sqrt(inPos.x*inPos.x + inPos.y*inPos.y + inPos.z*inPos.z))));
	output.texCoord = float2((atan2(inPos.z, inPos.x) / 3.141596535 + 1) / 2, inPos.y/100);

	return output;
}


float4 skyBox_ps(
	float2 pixelCoord : VPOS,

	uniform float4 viewport,
	uniform float4x4 matInvsProj,
	uniform float4x4 matInvsRota,

	uniform float time
	
) : COLOR
{
	//we need the direction of the ray of this fragment in world coordinates
	float2 fragCoord = pixelCoord / viewport.xy;
	fragCoord = (fragCoord - 0.5)*2.0;
	fragCoord.y *= -1;
	float4 device_normal = float4(fragCoord, 0.0, 1.0);
	float4 eye_normal = normalize(mul(matInvsProj, device_normal));
	float3 world_normal = normalize(mul(matInvsRota, eye_normal).xyz);

	//stars
	float starValue = snoise(world_normal * 80);
	if (starValue < 0.7)
	{
		starValue = 0;
	}
	else
	{
		starValue *= snoise(world_normal * 50);
	}

	starValue = abs(starValue);

	//clouds
	float2 lookup = float2(world_normal.x / (world_normal.y + 0.05) + time, world_normal.z / (world_normal.y + 0.05));

	float cloudValue = pow(snoise(lookup / 10), 4);
	cloudValue -= snoise(lookup) / 10;

	//moon
	//float moonValue = pow(dot(normalize(float3(1, 0.5, 0)), world_normal), 64);

	//mix
	float mix = (cloudValue + starValue);

	float4 color = float4(starValue, mix, starValue, 1);

	return color;
}