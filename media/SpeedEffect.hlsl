sampler2D input : register(s0);
uniform float blurfactor;

float4 speedeffect(float4 pos : POSITION, float2 uv : TEXCOORD0) : COLOR
{
	int nsamples = 20;
	float2 Center = { 0.5, 0.5 };
	float BlurStart = 1.0f;
	float BlurWidth = blurfactor;

	uv -= Center;
	float4 Blur = 0;
	for (int i = 0; i < nsamples; i++)
	{
		float scale = BlurStart + BlurWidth*(i / (float)(nsamples - 1));
		Blur += tex2D(input, uv * scale + Center);
	}
	Blur /= nsamples;
	return Blur;
}
