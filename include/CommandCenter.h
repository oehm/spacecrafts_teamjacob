 #ifndef __CommandCenter_h_
 #define __CommandCenter_h_ 

#include "ICollider.h"
#include "NavigationGraph.h"

/// Wall obstacle in the world
class CommandCenter: public ICollider
{
public:
	/// creates a wall from the startPos to the endPos
	CommandCenter(Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const Ogre::Vector3& position, const Ogre::String teamColor);

	virtual ~CommandCenter() {};

	virtual void onCollision(ICollider* collider) {};

	void createChareSpots();
	NavigationNode** getChargeSpots() { return mChargeSpots; };
private:
	Ogre::SceneNode* mShield;
	Ogre::Entity* mShieldEntity;

	static int msInstanceCounter;

	NavigationNode** mChargeSpots;

	Ogre::Vector3 mPosition;

	OgreBulletDynamics::RigidBody* mBody;
	OgreBulletCollisions::BoxCollisionShape *mShape;
	Ogre::SceneNode* mNode;
};


#endif